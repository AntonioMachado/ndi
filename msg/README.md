# Module Messages

Mircoservice chargé des messages.
Fourni :
- lister les conversations
- lister les messages d'une conversation
- poster un message dans une conversation
