from .app import db
import json


class Conversation(db.Model):

    id = db.Column(db.Integer(), primary_key=True)
    messages = db.relationship('Message', lazy=False)

    def toJSON(self):
        return {'id': self.id, 'messages': [m.toJSON() for m in self.messages]}


class Message(db.Model):

    id = db.Column(db.Integer(), primary_key=True)
    poster_name = db.Column(db.String(50))
    conversation_id = db.Column(db.Integer, db.ForeignKey('conversation.id'), nullable=False)
    message = db.Column(db.String(500))

    def toJSON(self):
        return {'id': self.id,
                'message': self.message,
                'poster': self.poster_name,
                'conversation_id': self.conversation_id
                }

db.create_all()