from messages.models import Conversation, Message
from .app import app, db, api
from flask import render_template, redirect, url_for, request, jsonify
from flask_restful import Resource


class ConversationAPI(Resource):

    def get(self, id):
        return Conversation.query.get(id).toJSON()

    def post(self, id):
        conv = Conversation.query.get(id)
        if not conv:
            conv = Conversation()
            conv.id = id
            db.session.add(conv)
        m = Message()
        m.conversation_id = id
        m.message = request.json['content']
        m.poster_name = request.json['poster']
        db.session.add(m)
        db.session.commit()


class ConversationListAPI(Resource):

    def get(self):
        return {'conversations': [c.toJSON() for c in Conversation.query.all()]}


api.add_resource(ConversationListAPI, '/conv')
api.add_resource(ConversationAPI, '/conv/<id>')
