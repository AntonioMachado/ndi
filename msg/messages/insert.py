from messages.models import Conversation, Message


def insert_data(db):
    try:
        conv = Conversation()
        conv.id = 0
        db.session.add(conv)
        for text in ["Bonjour", "Au revoir"]:
            m = Message()
            m.poster_name = 'Jean-Cloud'
            m.message = text
            m.conversation_id = 0
            db.session.add(m)
        db.session.commit()
    except:
        pass