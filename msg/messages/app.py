from flask import Flask
from flask_login import LoginManager
from flask_script import Manager
from flask_sqlalchemy import SQLAlchemy
from flask_restful import Api

app = Flask(__name__)
app.debug = True

app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql+psycopg2://groot:groot@db:5432/auth'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True

db = SQLAlchemy(app)

api = Api(app)

login_manager = LoginManager(app)
login_manager.login_view = "login"
app.config['SECRET_KEY'] = "Notre cle"

manager = Manager(app)

db.create_all()

from .insert import insert_data
insert_data(db)
