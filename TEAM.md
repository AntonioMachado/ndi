### Equipe

### Backend :
Baptiste Barbieri baptiste.barbieri@etu.univ-orleans.fr
Anass Hounnite anass.hounite@etu.univ-orleans.fr
Alexandre James alexandre.james@etu.univ-orleans.fr
Sebastien Rivault sebastien.rivault@etu.univ-orleans.fr
Frederik Voigt frederik.voigt@etu.univ-orleans.fr

### Frontend :
Ludovik Ellie ludovik.ellie@etu.univ-orleans.fr
Antonio Machado - chef de projet antonio-vasco.machado@etu.univ-orleans.fr
Maxime Mercier maxime.mercier@etu.univ-orleans.fr
