# Jean-Cloud à la rescousse !

Ceci est un bot type Akinator développé dans le cadre de la nuit de l'info 2019 dans le but d'apporter aux etudiants en galère des informations diverses facilement accessibles.

**Important**: L'appli est à visionner sur un écran format mobile pour une exprérience optimale.

## Scénario d'utilisation simple : guidage dans la FAQ

Jean-Cloud vous pose une question : répondez-y par oui ou non en cliquant sur un des boutons. Il vous posera d'autres questions afin de vous guider vers l'item de FAQ adapté.

## Fonctionalités :

- Un bot type Akinator accueille l'utilisateur et lui pose des questions afin de le diriger sur:
    - une entrée de FAQ pertinente
    - un chat avec un membre compétent
- Le bot est capable de fournir tout les entrées de la FAQ de l'Université de Grenoble.

## Installation

```
$ docker-compose build
$ docker-compose up
```

## Live Demo

[C'est ici que ça se passe !](http://51.83.253.190)

### Auteurs

Baptiste Barbieri
Ludovik Ellie
Anass Hounnite
Alexandre James
Antonio Machado
Maxime Mercier
Sebastien Rivault
Frederik Voigt