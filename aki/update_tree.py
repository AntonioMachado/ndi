import requests

from bs4 import BeautifulSoup

URL_BASE = "http://ici-grenoble.org"
URL = "/infospratiques/par-themes.php"
ANS_URL = "/infospratiques/"
EXPLORE = "sujets"
QUESTION = "sectionQR"
ANSWER = "principal"

def request_themes_url():
    html = requests.get(URL_BASE + URL).text
    page = BeautifulSoup(html, "lxml")
    nav = page.find("nav", {"id": EXPLORE})
    themes_urls = nav.find_all(href=True)
    return themes_urls

def request_theme(url):
    req = requests.get(URL_BASE+url)
    req.encoding = 'utf-8'
    page = BeautifulSoup(req.text, 'html.parser')
    section = page.find("section", {"id": QUESTION})
    return None if section is None else section.find_all("a", href=True)

def request_answer(url):
    req = requests.get(URL_BASE+ANS_URL+url)
    req.encoding = 'utf-8'
    page = BeautifulSoup(req.text, 'html.parser')
    section = page.find("section", {"id": ANSWER})
    return section.get_text(strip=True)


urls = request_themes_url()
questions = []
for a in urls[:1]:
    q = request_theme(a['href'])
    if q is not None :
        questions.extend(q)

QA = {}
for a in questions:
    QA[a.get_text(strip=True)] = request_answer(a['href'])

import nltk
from nltk.stem.snowball import FrenchStemmer
from nltk.corpus import stopwords
from collections import Counter
from questions import questions

stem_word = {}

def stem_words(words):
    stemmed_words = []
    stemmer = FrenchStemmer()
    for word in words:
        stemmed_word=stemmer.stem(word)
        stemmed_words.append(stemmed_word)
        stem_word[stemmed_word] = word
    stemmed_words.sort()
    return stemmed_words

def filter_stopwords(text,stopword_list):
    words=[w.lower() for w in text]
    filtered_words = []
    for word in words:
        if word not in stopword_list and word.isalpha() and len(word) > 1:
            filtered_words.append(word)
    filtered_words.sort()
    return filtered_words

def get_stop_words(tokens):
    filtered_words = filter_stopwords(tokens, stopwords.words('french'))
    return stem_words(filtered_words)

tok_kw = {}
for q, r in QA.items():
    q_token = nltk.word_tokenize(q)
    if q_token :
        values = [x for x in get_stop_words(q_token) if x in questions.keys()]
        if values:
            tok_kw[q] = values 
