from flask import Flask, jsonify, request
from flask_restful import Resource, Api
from akinator import get_response

app = Flask(__name__)
api = Api(app)

class AkinatorAPI(Resource):
    def get(self):
        return jsonify(get_response({"vélo":False}))

    def post(self):
        print(request.json)
        criteria=request.json['list']
        return jsonify(get_response(criteria))

api.add_resource(AkinatorAPI, '/')

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
