# Module Aki

Microservice stateless chargé de la logique de l'Akinator-like.
Fourni une route qui renvoie la prochaine question à poser à l'utilisateur.
S'initialise à partir de la FAQ en ligne de l'Université de Grenoble.