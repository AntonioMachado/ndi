q_concerne = "Votre question concerne t-elle les {} ?"
q_voulez = "Voulez vous {} quelque chose ?"

questions = {
    'vélo': q_concerne, 
    'voitur': q_concerne, 
    'apprendr': q_voulez, 
    'répar': q_voulez, 
    'lou': q_voulez, 
    'développ': q_voulez, 
    'vignet': q_concerne, 
    'bus': q_concerne, 
    'fraudent': q_concerne, 
    'tram': q_concerne, 
    'accident': q_concerne, 
    'bouchon': q_concerne, 
    'port': q_concerne, 
    'achet': q_voulez, 
    'transport': q_concerne, 
    'blablacar': q_concerne, 
    'pétrol': q_concerne, 
    'déplac': q_voulez, 
    'fauteuil': q_concerne, 
    'sort': q_concerne, 
    'voyag': q_voulez, 
    'ski': q_voulez, 
    'train': q_concerne, 
    'déménag': q_voulez
}

def get_question(stem, word):
    return questions.get(stem).format(word)