from update_tree import tok_kw, stem_word, QA
from collections import Counter
from questions import questions, get_question

def get_possible_question(criteria):
    criteria_t = [c for c in criteria.keys() if criteria[c]]
    criteria_f = [c for c in criteria.keys() if not criteria[c]]
    trimmed = {
        k: [x for x in v if x not in criteria.keys() and x in questions.keys()]
        for k, v in tok_kw.items()
            if all([c for c in criteria_t if c in v])
            and not any([c for c in criteria_f if c in v])
        }
    return trimmed

def find_most_common(counter):
    i = 0
    found = False
    stem = None
    while i < len(counter) and not found:
        stem = counter.most_common(i+1)[i][0]
        if stem in questions.keys():
            found = True
        i += 1
    return stem

def get_response(criteria):
    answers = get_possible_question(criteria)
    cpt = Counter()
    for v in answers.values():
        cpt += Counter(v)
    print(answers)
    print(len(answers))
    possible_answer = None
    stem = None
    question = None
    ans_sort = min(answers.keys(), key=lambda x : len(answers[x]))
    if not answers[ans_sort]:
        possible_answer = {x:QA[x] for x in answers.keys() if not answers[x]}
    # possible_answer = sorted[]
    else:
        stem = find_most_common(cpt)
        question = get_question(stem, stem_word[stem])
    return {
        "question": question,
        "list": criteria,
        "keyword": stem,
        "possible_answer": list(possible_answer.items()) if possible_answer is not None else None
    }
