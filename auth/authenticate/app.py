from flask import Flask
from flask_login import LoginManager
from flask_script import Manager
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.debug = True

# //user:pwd@nom_imagedocker(db)/(nomDb)host
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql+psycopg2://groot:groot@ndi_db_1:5432/auth'  # File-based SQL database
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True

db = SQLAlchemy(app)

login_manager = LoginManager(app)
login_manager.login_view = "login"
app.config['SECRET_KEY'] = "Notre cle"

manager = Manager(app)

db.create_all()


# @app.route("/")
# def hello():
#   return "fndzjpfbndkmsnfkmdqnkm"

if __name__ == "__main__":
    app.run(host='0.0.0.0')
