from .app import app, db
from pprint import pprint
from .models import UserEntity
from flask import render_template, redirect, url_for, request, jsonify
from flask_login import login_user, current_user, login_required, logout_user
from hashlib import sha256


@app.route("/", methods=['GET'])
def debug():
    return "Bonjour a tous"


@app.route("/inscription", methods=['POST'])
def register():
    content = request.json
    user = UserEntity()
    username = content['username']
    user.username = content['username']
    user.idphoto = content['idphoto']
    if UserEntity.getByUsername(UserEntity, user.username) != None:
        return jsonify(isError=False, message="Unhautorized, username already exist", statusCode=401)
    user.password = shasum(content['password'].encode('utf-8'))
    db.session.add(user)
    db.session.commit()
    return jsonify(isError=False, message="Success", statusCode=200,
                   data={"login": user.username, "idphoto": user.idphoto})


@app.route("/login", methods=['POST'])
def login():
    content = request.json
    user = UserEntity()
    user.username = content['username']
    user.password = shasum(content['password'].encode('utf-8'))
    checkU = UserEntity.getByUsername(UserEntity, user.username)
    if checkU != None:
        if checkU.password == user.password:
            connected = login_user(user)
            return jsonify(isError=False, message='Success', statusCode=200, data=connected)
    else:
        return jsonify(isError=False, message='No_UserEntity_found', statusCode=404)


def shasum(value_to_hash):
    passwc = sha256()
    passwc.update(value_to_hash)
    return passwc.hexdigest()
