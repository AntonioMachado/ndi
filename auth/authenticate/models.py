from flask_login import UserMixin

from .app import db, login_manager


@login_manager.user_loader
def load_user(username):
    return UserEntity.query.get(username)


class UserEntity(db.Model, UserMixin):

    username = db.Column(db.String(50), primary_key=True)
    password = db.Column(db.String(64))
    idphoto = db.Column(db.Integer)


    def get_id(self):
        return self.username

    def getByUsername(self,username):
        return UserEntity.query.get(username)


db.create_all()