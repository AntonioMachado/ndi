import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {AlertController, ModalController} from "@ionic/angular";
import {Router} from "@angular/router";
import {AuthentificationService} from "../../services/authentification.service";
import {UserDto} from "../../model/UserDto";
import {ResponseDto} from "../../model/ResponseDto";
import {AuthResultDto} from "../../model/AuthResultDto";

@Component({
    selector: 'app-register',
    templateUrl: './register.page.html',
    styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

    public registerForm: FormGroup;

    errorMessages = {
        confirmPassword: [
            {type: 'notSame', message: 'Le mot de passe ne correspond pas'}
        ]
    };

    constructor(
        private authService: AuthentificationService,
        private alertController: AlertController,
        private modalCtrl: ModalController,
        public formBuilder: FormBuilder) {
    }

    ngOnInit() {
        this.registerForm = this.formBuilder.group({
            login: ['', Validators.required],
            password: ['', Validators.required],
            photoId: ['', Validators.required],
            confirmPassword: new FormControl('', [Validators.required])
        }, {validators: this.checkPasswords});
    }

    /**
     * Méthode de validation de formulaire qui va permettre de vérifier que le mdp et le mdp de confirmation sont identiques
     * @param group Le formulaire qui va être validé
     */
    checkPasswords(group: FormGroup) {
        const password = group.get('password');
        const confirmPassword = group.get('confirmPassword');
        // Si les deux mdps sont différents on set l'errreur notSame
        if (password.value !== confirmPassword.value) {
            confirmPassword.setErrors({notSame: true});
        } else {
            confirmPassword.setErrors(null);
        }
        return null;
    }

    public register() {
        const userDto: UserDto = {
            username: this.registerForm.get('login').value,
            password: this.registerForm.get('password').value,
            idphoto: this.registerForm.get('photoId').value
        }
        console.log(userDto);
        this.authService.postRegister(userDto).subscribe(response => {
            this.registerResult(response);
        });
    }

    /**
     * Permet d'afficher une alert box contenant le message d'erreur passé en paramètre
     * @param response
     */
    private registerError(response: ResponseDto<AuthResultDto>) {
        this.modalCtrl.dismiss().then(() =>
            this.alertController.create({
                header: 'Echec',
                message: response.message,
                buttons: ['Ok']
            }).then(alert => alert.present()));
        this.registerForm.reset();
    }

    private registerResult(response: ResponseDto<AuthResultDto>) {
        if (response.isError) {
            this.registerError(response);
        } else {
            console.log(response.data);
            const user: AuthResultDto = {
                login: response.data.login,
                idphoto: response.data.idphoto
            };
            this.authService.setUser(user);
            this.modalCtrl.dismiss().then(() =>
                this.alertController.create({
                    header: 'Succès',
                    message: 'Inscription réussie !',
                    buttons: ['Ok']
                }).then(alert => alert.present()));
        }
    }

    dismissModal() {
        this.modalCtrl.dismiss();
    }
}
