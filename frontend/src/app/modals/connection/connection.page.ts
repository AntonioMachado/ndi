import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AlertController, ModalController} from "@ionic/angular";
import {AuthentificationService} from "../../services/authentification.service";
import {Router} from "@angular/router";
import {UserDto} from "../../model/UserDto";
import {ResponseDto} from "../../model/ResponseDto";
import {AuthResultDto} from "../../model/AuthResultDto";

@Component({
    selector: 'app-connection',
    templateUrl: './connection.page.html',
    styleUrls: ['./connection.page.scss'],
})
export class ConnectionPage implements OnInit {

    public loginForm: FormGroup;

    constructor(
        private authService: AuthentificationService,
        private alertController: AlertController,
        private modal: ModalController,
        public formBuilder: FormBuilder) {
        this.loginForm = this.formBuilder.group({
            login: ['', Validators.required],
            password: ['', Validators.required]
        });
    }


    ngOnInit() {
    }

    private connexionResult(response: ResponseDto<AuthResultDto>) {
        if (response.isError) {
            this.connectionError(response)
        } else {
            const user: AuthResultDto = {
                login: response.data.login,
                idphoto: response.data.idphoto
            };
            this.authService.setUser(user);
            this.modal.dismiss().then(() => this.alertController.create({
                header: 'Succès',
                message: 'Connexion réussie !',
                buttons: ['Ok']
            }).then(alert => alert.present()));
        }
    }

    /**
     * Permet d'afficher une alert box contenant le message d'erreur passé en paramètre
     * @param response
     */
    private connectionError(response: ResponseDto<AuthResultDto>) {
        this.alertController.create({
            header: 'Echec',
            message: response.message,
            buttons: ['Ok']
        }).then(alert => alert.present());
        this.loginForm.reset();
    }

    public connection() {
        const authRequest: UserDto = new UserDto(this.loginForm.get('login').value, this.loginForm.get('password').value);
        this.authService.postLogin(authRequest).subscribe(response => {
            this.connexionResult(response);});
    }

    dismissModal() {
        this.modal.dismiss();
    }
}
