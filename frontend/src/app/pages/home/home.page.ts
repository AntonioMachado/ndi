import { Platform } from '@ionic/angular';
import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import {ChatTab} from "../../model/ChatTab";

interface AkinatorReq {
  question: String;
  list?: {};
  keyword?: any;
  possible_answer?: any[];
}
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage{
  chatTab: ChatTab;

  currentQuestion: BehaviorSubject<AkinatorReq>;

  constructor(public platform: Platform, private httpClient: HttpClient) {
  }

  ngOnInit(): void {
    this.currentQuestion = new BehaviorSubject({
      question: '',
    });
    this.submit(null);
    this.currentQuestion.subscribe(data => {
      console.log(data);
    });
    this.chatTab = new ChatTab();
    this.chatTab.imageProfil = "https://gravatar.com/avatar/dba6bae8c566f9d4041fb9cd9ada7741?d=identicon&f=y";
    this.chatTab.date="05/12/19";
    this.chatTab.interlocuteur="jean-cloud";
    this.chatTab.message="Bonjour, je m'appelle jean-cloud";
    this.chatTab.nbMsgNonLu = 1;
  }

  public async submit(b: boolean) {
    let obj = {};
    if (this.currentQuestion.value && this.currentQuestion.value.keyword) {
      this.currentQuestion.value.list[this.currentQuestion.value.keyword] = b;
      obj = {
        list: this.currentQuestion.value.list
      };
    } else {
      obj = {
        list: {}
      };
    }
    this.httpClient.post<AkinatorReq>('http://51.83.253.190/akinator/', obj).subscribe(data => {
      this.currentQuestion.next(data);
    });
  }

  rickroll(){
    window.open("'https://www.youtube.com/watch?v=dQw4w9WgXcQ&list=PLahKLy8pQdCM0SiXNn3EfGIXX19QGzUG3", '_system');
  }
}
