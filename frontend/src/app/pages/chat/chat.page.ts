import { ChatModalPage } from 'src/app/pages/chat-modal/chat-modal.page';
import { ModalController } from '@ionic/angular';
import { ChatTab } from './../../model/ChatTab';
import { Component, OnInit } from '@angular/core';
import {Message} from "../../model/Message";
import {DatePipe} from "@angular/common";
import {MessagesService} from "../../services/messages.service";



@Component({
  selector: 'app-chat',
  templateUrl: './chat.page.html',
  styleUrls: ['./chat.page.scss'],
})
export class ChatPage implements OnInit {

  chatTabs: ChatTab[];

  constructor(private msgService : MessagesService, private modalCtrl: ModalController) { }

  ngOnInit() {
    this.chatTabs = [{
      imageProfil: null,
      interlocuteur: "Alex",
      message: "Slt mec",
      nbMsgNonLu: 12,
      date: "Hier",
    }];
  }

  async presentModal(chat: any){
    const modal = await this.modalCtrl.create({
      component: ChatModalPage,
      componentProps: {
        'chat': chat
      }
    });

    await modal.present();
  }
}
