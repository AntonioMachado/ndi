import { ChatModalPageModule } from './../chat-modal/chat-modal.module';
import { ComponentsModule } from './../../components/components.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ChatPage } from './chat.page';
import { RouterModule } from '@angular/router';
import { ChatPageRoutingModule } from './chat-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ChatPageRoutingModule,
    ComponentsModule,
    ChatModalPageModule,
    RouterModule.forChild([
      {
        path: '',
        component: ChatPage
      }
    ])
  ],
  declarations: [ChatPage]
})
export class ChatPageModule {}
