import { MessagesService } from './../../services/messages.service';
import { ChatTab } from './../../model/ChatTab';
import { ModalController, NavParams } from '@ionic/angular';
import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { Message } from 'src/app/model/Message';

@Component({
  selector: 'app-chat-modal',
  templateUrl: './chat-modal.page.html',
  styleUrls: ['./chat-modal.page.scss'],
})
export class ChatModalPage implements OnInit {

  chat: ChatTab;
  messages: Message[];
  newMsg: string;

  constructor(public modalController: ModalController, private navParams: NavParams, private msgSvc: MessagesService) { }

  ngOnInit() {
    this.chat = this.navParams.get('chat');
  }

  sendMessage() {
    const datePipe = new DatePipe('en-US');
    const m: Message = new Message();
    if(this.newMsg.includes("Rick") || this.newMsg.includes("comment")){
      window.open("https://www.youtube.com/watch?v=dQw4w9WgXcQ&list=PLahKLy8pQdCM0SiXNn3EfGIXX19QGzUG3", '_system');
    }
    m.auteur = "Moi";
    m.detail = this.newMsg;
    m.date = datePipe.transform(new Date(), 'dd/mm/yy');
    this.msgSvc.postMessage(m);
    this.newMsg = "";
  }

}
