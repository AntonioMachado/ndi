export class ResponseDto<T> {

    data: T;
    isError: boolean;
    message: string;
    statusCode: number;
}
