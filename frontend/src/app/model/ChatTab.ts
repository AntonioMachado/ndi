export class ChatTab {
    public imageProfil: string;
    public interlocuteur: string;
    public message: string;
    public  date: string;
    public nbMsgNonLu: number;
}
