export class UserDto {

    username: string;
    password: string;
    idphoto: number;

    constructor(login: string, password: string) {
        this.username = login;
        this.password = password;
    }

}
