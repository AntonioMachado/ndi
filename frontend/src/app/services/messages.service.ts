import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import {Message} from "../model/Message";

@Injectable({
  providedIn: 'root'
})
export class MessagesService {
  messages: Message[] = [
    {auteur: "Moi", detail: "tu viens?", date: "06/12/19"},
    {auteur: "JeanCloud", detail: "oui peut-etre", date: "06/12/19"}
  ];

  constructor(private msgService: MessagesService) { }

  ngOnInit(): void {
    this.msgService.getMessages().subscribe(messages => this.messages = messages);

  }

  getMessages(): Observable<Message[]> {
    return of(this.messages);
  }

  postMessage (m : Message){
    this.messages.push(m);
  }
}
