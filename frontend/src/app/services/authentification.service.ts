import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {UserDto} from "../model/UserDto";
import {Observable} from "rxjs";
import {ResponseDto} from "../model/ResponseDto";
import {AuthResultDto} from "../model/AuthResultDto";

@Injectable({
    providedIn: 'root'
})
export class AuthentificationService {

    private readonly apiUrl = 'http://51.83.253.190/auth';
    private user:AuthResultDto;

    constructor(private http: HttpClient) {
    }

    public postLogin(user: UserDto): Observable<ResponseDto<AuthResultDto>>  {
        return this.http.post<ResponseDto<AuthResultDto>>(this.apiUrl + '/login', user);
    }

    public postRegister(user: UserDto): Observable<ResponseDto<AuthResultDto>> {
        return this.http.post<ResponseDto<AuthResultDto>>(this.apiUrl + '/inscription', user);
    }

    public setUser(user: AuthResultDto) {
        this.user = user;
    }

    public getUser(): AuthResultDto {
        return this.user
    }

    public userIsConnected(): boolean {
        return this.user !== undefined;
    }
}
