import {Component, OnInit} from '@angular/core';
import {AuthentificationService} from "../../services/authentification.service";
import {ModalController} from "@ionic/angular";
import {RegisterPage} from "../../modals/register/register.page";
import {ConnectionPage} from "../../modals/connection/connection.page";
import {UserDto} from "../../model/UserDto";
import {AuthResultDto} from "../../model/AuthResultDto";

@Component({
    selector: 'app-menu',
    templateUrl: './menu.component.html',
    styleUrls: ['./menu.component.scss'],
})
export class MenuComponent implements OnInit {

    public user: AuthResultDto;

    constructor(private authentificationService: AuthentificationService,
                private modalController: ModalController) {
    }

    ngOnInit() {
        this.user = new AuthResultDto();
        if (this.authentificationService.userIsConnected()) {
            this.user = this.authentificationService.getUser();
        }
    }

    public userIsConnected() {
        return this.authentificationService.userIsConnected();
    }

    public async getModal(page: any) {
        return await this.modalController.create({
            component: page
        });
    }

    public async connect() {
        const modal = await this.getModal(ConnectionPage);
        return await modal.present();
    }

    public async register() {
        const modal = await this.getModal(RegisterPage);
        return await modal.present();
    }
}
