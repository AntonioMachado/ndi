import {Component, Input, OnInit} from '@angular/core';
import {ChatTab} from "../../model/ChatTab";
import { ModalController } from '@ionic/angular';
import { ChatModalPage } from 'src/app/pages/chat-modal/chat-modal.page';

@Component({
  selector: 'app-chat-tab',
  templateUrl: './chat-tab.component.html',
  styleUrls: ['./chat-tab.component.scss'],
})
export class ChatTabComponent implements OnInit {
  @Input() chat : ChatTab;

  constructor(private modalCtrl: ModalController) { }

  ngOnInit() {}

  

}
